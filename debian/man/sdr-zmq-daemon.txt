NAME
  sdr-zmq-daemon - ZeroMQ daemon for Software Defined Radios (SDRs)

SYNOPSIS
  sdr-zmq-daemon

DESCRIPTION

  Simply start with no options. It will connect to your software defined radio, and start providing IQ data over ZeroMQ on address tcp://127.0.0.1:10444 and provide a configuration interface on tcp://127.0.0.1:10445.
  It is intended to be used together with grdab (Using the 'grdab -z' option).

  All SDRs supported by gr-osmosdr can be used with sdr-zmq-daemon. It has been verified to work with RTL-SDR, HackRF and USRP B200.

  It is not a 'true daemon' yet, so you will have to use 'screen' or similar to get it to hide in the background.

OPTIONS

  No options available
 
SEE ALSO
  grdab(1)


AUTHOR
  This manual page was written by Ruben Undheim <rubund@debian.org> for the Debian project (and may be used by others).




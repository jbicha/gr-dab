Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gr-dab
Source: https://github.com/andrmuel/gr-dab

Files: *
Copyright: 2004,2007,2010,2013 Free Software Foundation, Inc.
           2008-2011 Andreas Müller <am@0x7.ch>
           2015,2018 Ruben Undheim <ruben.undheim@gmail.com>
           2017 Moritz Luca Schmid <luca.moritz.schmid@gmail.com>
                - Communications Engineering Lab (CEL) / Karlsruhe Institute of Technology (KIT)
License: GPL-3+

Files: lib/fec/*
Copyright: 2002-2004 Phil Karn, KA9Q
License: LGPL-2.1

Files: lib/reed_solomon_decode_bb_impl.*
Copyright: 2002 Phil Karn, KA9Q
           2017 Moritz Luca Schmid <luca.moritz.schmid@gmail.com>
                - Communications Engineering Lab (CEL) / Karlsruhe Institute of Technology (KIT)
License: GPL-3+

Files: lib/firecode_check_bb_impl.*
       lib/mp4_decode_bs_impl.*
Copyright: 2013,2017 Jan van Katwijk (Lazy Chair Computing) <J.vanKatwijk@gmail.com>
           2017 Moritz Luca Schmid <luca.moritz.schmid@gmail.com>
                - Communications Engineering Lab (CEL) / Karlsruhe Institute of Technology (KIT)
License: GPL-3+

Files: lib/mp2_decode_bs_impl.*
Copyright: 2006-2013 Martin J. Fiedler <martin.fiedler@gmx.net>
           2017 Moritz Luca Schmid <luca.moritz.schmid@gmail.com>
                - Communications Engineering Lab (CEL) / Karlsruhe Institute of Technology (KIT)
License: zlib_license

Files: lib/neaacdec.h
Copyright: 2003-2005 M. Bakker, Nero AG, http://www.nero.com
License: GPL-2+
Note: The "appropriate copyright message" mentioned in section 2c of the GPLv2
      must read: "Code from FAAD2 is copyright (c) Nero AG, www.nero.com"

Files: debian/*
Copyright: 2018  Ruben Undheim <ruben.undheim@gmail.com>
License: GPL-3+


License: GPL-3+
 This package is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in "/usr/share/common-licenses/GPL-3".


License: zlib_license
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
   1. The origin of this software must not be misrepresented; you must not
      claim that you wrote the original software. If you use this software
      in a product, an acknowledgment in the product documentation would
      be appreciated but is not required.
   2. Altered source versions must be plainly marked as such, and must not
      be misrepresented as being the original software.
   3. This notice may not be removed or altered from any source
      distribution.


License: GPL-2+
 This package is free software: you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2".


License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; version
 2.1 of the license
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License version 2.1 can be found in
 "/usr/share/common-licenses/LGPL-2.1".
